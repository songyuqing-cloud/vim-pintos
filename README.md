 # VIM-PINTOS

  vim configuration for pintos project. (**Optional**)

  Prerequisites
  -------------
  - Putty font configuration
    - Window -> Appearance -> Font settings, press 'Change..' button and change font to **Consolas**.
#
  - Installation of ctags
    - ctags is already installed in uni server.
    - If you want to do pintos on your local machine (**Mac** or **Ubuntu**), please install ctags.
    - Command
      - Mac:
        ``` 
        $brew install ctags
        ```
      - Ubuntu: 
        ```
        $sudo apt-get install ctags 
        ```

  VIM Installation
  ------------
  - Follow below commands on **home path**.
  ```
  ~]$ git clone https://gitlab.com/2019f-os-vim/vim-pintos.git
  ~]$ git clone https://github.com/VundleVim/Vundle.vim.git ~/vim-pintos/bundle/Vundle.vim
  ~]$ source ~/vim-pintos/setup.sh
  ```

  ctags
  -----
  - A programming tool that generates a tag file of names found in source and header files
    which allows definitions to be quickly.
  - **Settings**:
    - Generate tags file.
    - {pintos PATH} means a location that src/ directory exists.
      ```
      {pintos PATH}]$ ctags -R .
      ```
    - Then, you can see a new 'tags' file in the path.
    - In ~/.vim/after/ftplugin/c.vim (e.g, set tags=~/pintos/tags)
      ```
      set tags=${pintos PATH}/tags 
      ```
  - Basic Usage:
    - Go to definition: **F5**
    - Go back to the code: **F6**
  - **Caution**: If 'tags not found' error message is shown, please generate tags file again.

  Default Settings
  ----------------
  - Show line number
    - Toggle Mapping Key: **F3**
  - Enable to do mouse-scroll in normal mode while copy & paste using the mouse are allowed in insert mode.

  Plugins
  -------
  - vim-colorschemes
    - A colorschemes for vi editor.
    - References: https://github.com/flazz/vim-colorschemes
    - USAGE: colorscheme [colorscheme name] (e.g, molokai, jellybeans, wombat) 
    - Default: In ~/.vim/after/ftplugin/c.vim 
      ```
      colorscheme molokai 
      ```
    - If you want to apply colorscheme to all files, put 'colorscheme [name]' to ~/.vim/colors.vim
#

  - NERDTree
    - A file and directory tree for vi editor.
    - References: https://github.com/scrooloose/nerdtree
    - How to use: https://medium.com/usevim/nerd-tree-guide-bb22c803dcd2
    - Toggle Mapping Key: **F1**
    - Open the selected file in a horizontal/vertical split window: **i** / **s**
    - Move cursor to splited window: **Ctrl + w** or **mouse click**
    - Open the selected file in a new tab: **t** or mouse **double-click**.
    - Keymap for buffer in tab
      - Move next(right) buffer: :bn
      - Move prev(left) buffer: :bp
      - Delete current buffer: :bd
    - Window size can be adjusted with mouse drag.
#

  - tagbar
    - A plugin that provides an easy way to get an overview of its code structure.
      You can see defined struct, variables, functions, and navigate to the code.
    - References: https://github.com/majutsushi/tagbar
    - Prerequisite: ctags
    - Toggle Mapping Key: **F2**
    - Move the window: **Ctrl + ww** or **mouse click**
    - Window size can be adjusted with mouse drag.
#

  - NeoComplete
    - A plugins for auto completion.
    - References: https://github.com/Shougo/neocomplete.vim
    - Press **TAB** to keep recommending syntax.
    - Press **ENTER** to complete.
#

  - ctrlp.vim
    - A plugin for searching files and directories in vi editor window.
    - References: https://github.com/kien/ctrlp.vim
    - Limitation: only searching files and directories in the current path.
    - Mapping Key: **Ctrl + p**
#

  - grep.vim
    - A plugin for using grep command in vi editor window.
    - Mapping Key: **F4**
    - Commands
      - Quickfix list open: :copen 
      - Move next file in Quickfix list: :cnext (cn) 
      - Move prev file in Quickfix list: :cprev (cp) 
#
 
  - vim-smooth-scroll
    - Mapping Key: 
      - UP: **Ctrl + u** or **Ctrl + f**
      - DOWN: **Ctrl + d** or **Ctrl + b**
#

  - indentLine (default: enable)
    - References: https://github.com/Yggdroot/indentLine
    - Toggle: **Ctrl + i**
    - To disable: In ~/.vim/plugins.vim
      ```
      let g:indentLine_enabled = 0
      ```
