colorscheme molokai

" Syntastic settings
let g:syntastic_c_remove_include_errors = 1

" ctags
" example: set tags=~/pintos/tags
set tags=
